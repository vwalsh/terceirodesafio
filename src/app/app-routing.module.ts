import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GeralComponent} from './geral/geral.component';
import {CategoriaComponent} from './categoria/categoria.component';
import {MesComponent} from './mes/mes.component';

const routes: Routes = [
  { path: 'geral', component: GeralComponent },
  { path: 'categoria',  component: CategoriaComponent},
  { path: 'mes',  component: MesComponent},
  { path: '', redirectTo: '/geral', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
