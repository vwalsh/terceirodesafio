import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MonthPipe} from './pipes/month.pipe';
import { MesComponent } from './mes/mes.component';
import { GeralComponent } from './geral/geral.component';
import { CategoriaComponent } from './categoria/categoria.component';
import {MatListModule, MatTabsModule} from '@angular/material';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import { OrderModule } from 'ngx-order-pipe';

@NgModule({
  declarations: [
    AppComponent,
    MesComponent,
    GeralComponent,
    CategoriaComponent,
    MonthPipe
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatTabsModule,
    RouterModule,
    HttpClientModule,
    MatListModule,
    OrderModule
  ],
  providers: [MonthPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
