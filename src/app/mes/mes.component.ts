import { Component, OnInit } from '@angular/core';
import {Lancamento} from '../models/lancamento.model';
import {GastosService} from '../services/gastos.service';

@Component({
  selector: 'app-mes',
  templateUrl: './mes.component.html',
  styleUrls: ['./mes.component.css']
})
export class MesComponent implements OnInit {
  lancamentos: Lancamento[];

  constructor(
    private api: GastosService) {
  }

  ngOnInit() {
    this.getLancamentos();
  }

  getLancamentos() {
    this.api.getLancamentos().subscribe((lancamentos: Lancamento[]) => {
      this.lancamentos = lancamentos.reduce((acc, curr) => {
        let find = false;
        for (const lancamento of acc) {
          if (lancamento.mes_lancamento === curr.mes_lancamento) {
            lancamento.valor += curr.valor;
            find = true;
            break;
          }
        }
        if (!find) {
          acc.push(curr);
        }
        return acc;
      }, []);
    });
  }
}
