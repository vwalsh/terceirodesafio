import { Component, OnInit } from '@angular/core';
import {GastosService} from '../services/gastos.service';
import {Lancamento} from '../models/lancamento.model';
import {Categoria} from '../models/categoria.model';

@Component({
  selector: 'app-geral',
  templateUrl: './geral.component.html',
  styleUrls: ['./geral.component.css']
})
export class GeralComponent implements OnInit {

  lancamentos: Lancamento[];
  categorias: Categoria[];

  constructor(
    private api: GastosService ) { }

  ngOnInit() {
    this.getCategoria();
    this.getLancamentos();
  }

  getCategoria() {
    this.api.getCategorias().subscribe((categorias: Categoria[]) => {
      this.categorias = categorias;
    });
  }

  getLancamentos() {
    this.api.getLancamentos().subscribe((lancamentos: Lancamento[]) => {
      this.lancamentos = lancamentos;
    });
  }
}
