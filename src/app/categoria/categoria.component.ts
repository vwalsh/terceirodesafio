import { Component, OnInit } from '@angular/core';
import {Lancamento} from '../models/lancamento.model';
import {GastosService} from '../services/gastos.service';
import {Categoria} from '../models/categoria.model';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {
  lancamentos: Lancamento[];
  categorias: Categoria[];

  constructor(
    private api: GastosService) {
  }

  ngOnInit() {
    this.getLancamentos();
    this.getCategoria();
  }

  getLancamentos() {
    this.api.getLancamentos().subscribe((lancamentos: Lancamento[]) => {
      this.lancamentos = lancamentos.reduce((acc, curr) => {
        let find = false;
        for (const lancamento of acc) {
          if (lancamento.categoria === curr.categoria) {
            lancamento.valor += curr.valor;
            find = true;
            break;
          }
        }
        if (!find) {
          acc.push(curr);
        }
        return acc;
      }, []);
    });
  }

  getCategoria() {
    this.api.getCategorias().subscribe((categorias: Categoria[]) => {
      this.categorias = categorias;
    });
  }

  changeCategory(valor) {
    return this.categorias.find( x => x.id === valor).nome;
  }
}
