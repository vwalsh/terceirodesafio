import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'terceirodesafio';

  navLinks = [
    { path: 'geral', label: 'Lista Geral' },
    { path: 'mes', label: 'Por Mês' },
    { path: 'categoria', label: 'Por Categoria' },
  ];
}
